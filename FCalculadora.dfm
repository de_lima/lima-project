inherited frmCalculadora: TfrmCalculadora
  Caption = 'Calculadora'
  ClientHeight = 536
  ClientWidth = 334
  Constraints.MinHeight = 575
  Constraints.MinWidth = 350
  ExplicitWidth = 350
  ExplicitHeight = 575
  PixelsPerInch = 96
  TextHeight = 13
  object btnSeis: TSpeedButton [0]
    Left = 171
    Top = 289
    Width = 75
    Height = 75
    Caption = '6'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnSeisClick
  end
  object btnOito: TSpeedButton [1]
    Left = 97
    Top = 215
    Width = 75
    Height = 75
    Caption = '8'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnOitoClick
  end
  object btnCinco: TSpeedButton [2]
    Left = 97
    Top = 289
    Width = 75
    Height = 75
    Caption = '5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnCincoClick
  end
  object btnSubtracao: TSpeedButton [3]
    Left = 245
    Top = 141
    Width = 75
    Height = 75
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnSubtracaoClick
  end
  object btnQuatro: TSpeedButton [4]
    Left = 23
    Top = 289
    Width = 75
    Height = 75
    Caption = '4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnQuatroClick
  end
  object btnSoma: TSpeedButton [5]
    Left = 245
    Top = 215
    Width = 75
    Height = 75
    Caption = '+'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnSomaClick
  end
  object btnTres: TSpeedButton [6]
    Left = 171
    Top = 363
    Width = 75
    Height = 75
    Caption = '3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnTresClick
  end
  object btnDois: TSpeedButton [7]
    Left = 97
    Top = 363
    Width = 75
    Height = 75
    Caption = '2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnDoisClick
  end
  object btnUm: TSpeedButton [8]
    Left = 23
    Top = 363
    Width = 75
    Height = 75
    Caption = '1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnUmClick
  end
  object btnEnter: TSpeedButton [9]
    Left = 245
    Top = 362
    Width = 75
    Height = 150
    Caption = 'Enter'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnEnterClick
  end
  object btnVirgula: TSpeedButton [10]
    Left = 171
    Top = 437
    Width = 75
    Height = 75
    Caption = ','
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnVirgulaClick
  end
  object btnZero: TSpeedButton [11]
    Left = 23
    Top = 437
    Width = 149
    Height = 75
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnZeroClick
  end
  object btnNove: TSpeedButton [12]
    Left = 171
    Top = 215
    Width = 75
    Height = 75
    Caption = '9'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnNoveClick
  end
  object btnSete: TSpeedButton [13]
    Left = 23
    Top = 215
    Width = 75
    Height = 75
    Caption = '7'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnSeteClick
  end
  object btnDivisao: TSpeedButton [14]
    Left = 97
    Top = 141
    Width = 75
    Height = 75
    Caption = '/'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnDivisaoClick
  end
  object btnMultiplicacao: TSpeedButton [15]
    Left = 171
    Top = 141
    Width = 75
    Height = 75
    Caption = '*'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnMultiplicacaoClick
  end
  object btnPorcentagem: TSpeedButton [16]
    Left = 245
    Top = 289
    Width = 75
    Height = 75
    Caption = '%'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnPorcentagemClick
  end
  object btnLimpa: TSpeedButton [17]
    Left = 23
    Top = 141
    Width = 75
    Height = 75
    Caption = 'CE'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    OnClick = btnLimpaClick
  end
  object lblValorAnterior: TFLabel [18]
    Left = 313
    Top = 41
    Width = 6
    Height = 23
    Alignment = taRightJustify
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
  end
  inherited pnBarra: TPanel
    Width = 334
    TabOrder = 1
    ExplicitWidth = 334
    inherited btSair: TBitBtn
      TabStop = False
    end
  end
  object fedNumeros: TFEdit [20]
    Left = 24
    Top = 66
    Width = 295
    Height = 75
    Alignment = taRightJustify
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -45
    Font.Name = 'Tahoma'
    Font.Style = []
    Font.Quality = fqClearTypeNatural
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    DataType = ftFloat
    Aligment = taRightJustify
  end
  inherited ImageList1: TImageList
    Left = 355
    Top = 86
  end
end
