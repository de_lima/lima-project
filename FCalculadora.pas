unit FCalculadora;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Form_Base0, Vcl.Buttons,
  System.ImageList, Vcl.ImgList, CBitBtn, Vcl.StdCtrls, Vcl.ExtCtrls, FEdit,
  FLabel, ECComum;

type
  TfrmCalculadora = class(TfrmBase0)
    btnSeis: TSpeedButton;
    btnOito: TSpeedButton;
    btnCinco: TSpeedButton;
    btnSubtracao: TSpeedButton;
    btnQuatro: TSpeedButton;
    btnSoma: TSpeedButton;
    btnTres: TSpeedButton;
    btnDois: TSpeedButton;
    btnUm: TSpeedButton;
    btnEnter: TSpeedButton;
    btnVirgula: TSpeedButton;
    btnZero: TSpeedButton;
    btnNove: TSpeedButton;
    btnSete: TSpeedButton;
    btnDivisao: TSpeedButton;
    btnMultiplicacao: TSpeedButton;
    btnPorcentagem: TSpeedButton;
    btnLimpa: TSpeedButton;
    fedNumeros: TFEdit;
    lblValorAnterior: TFLabel;
    procedure btnLimpaClick(Sender: TObject);
    procedure btnZeroClick(Sender: TObject);
    procedure btnUmClick(Sender: TObject);
    procedure btnDoisClick(Sender: TObject);
    procedure btnTresClick(Sender: TObject);
    procedure btnQuatroClick(Sender: TObject);
    procedure btnCincoClick(Sender: TObject);
    procedure btnSeisClick(Sender: TObject);
    procedure btnSeteClick(Sender: TObject);
    procedure btnOitoClick(Sender: TObject);
    procedure btnNoveClick(Sender: TObject);
    procedure btnPorcentagemClick(Sender: TObject);
    procedure btnEnterClick(Sender: TObject);
    procedure btnVirgulaClick(Sender: TObject);
    procedure btnSomaClick(Sender: TObject);
    procedure btnSubtracaoClick(Sender: TObject);
    procedure btnMultiplicacaoClick(Sender: TObject);
    procedure btnDivisaoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
  var vConstante, vVariavel, vResult :Float64;
      vOperacao, vAux: Integer;
    { Private declarations }
  public
    { Public declarations }
  protected

  end;

var
  frmCalculadora: TfrmCalculadora;

implementation

{$R *.dfm}


procedure TfrmCalculadora.btnCincoClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + (Sender as TSpeedButton).Caption;
end;

procedure TfrmCalculadora.btnDivisaoClick(Sender: TObject);
begin
  inherited;
  if fedNumeros.Text = '' then begin
    fedNumeros.SetFocus;
    Abort;
  end;

  vConstante := StrToFloat(fedNumeros.Text);
  vVariavel := 0;
  fedNumeros.Text := '';
  vOperacao := 1;
  lblValorAnterior.Caption := FloatToStr(vResult);
end;

procedure TfrmCalculadora.btnDoisClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + (Sender as TSpeedButton).Caption;
end;

procedure TfrmCalculadora.btnEnterClick(Sender: TObject);
begin
  inherited;
  // Valida se o Edit possui valor para efetuar c�lculo
  if fedNumeros.Text = '' then begin
    TMensagem.Alertar('Valor Inv�lido');
    Abort;
  end;

  // Faz o c�lculo do que foi informado
  case vOperacao of
    1: // DIVIS�O SIMPLES
    begin
      if vVariavel = 0 then begin
        vVariavel := StrToFloat(fedNumeros.Text);
      end;

      // Executa a opera��o
      vResult := vConstante / vVariavel;
      fedNumeros.Text := FloatToStr(vResult);
    end;

    2:
    begin
      if vVariavel = 0 then begin
        vVariavel := StrToFloat(fedNumeros.Text);
      end;

      // Executa a opera��o
      vResult := vConstante * vVariavel;
      fedNumeros.Text := FloatToStr(vResult);
    end;

    3: // SUBTRA��O SIMPLES
    begin
      if vResult <> 0 then begin
        lblValorAnterior.Caption := FloatToStr(vResult) + ' - ' + FloatToStr(vVariavel);
        vResult := vResult - vVariavel;
        fedNumeros.Text := FloatToStr(vResult);
      end;

      if vVariavel = 0 then begin
        // Passa Valor para a Vari�vel
        vVariavel := StrToFloat(fedNumeros.Text);
        // Executa a opera��o
        vResult := (vConstante - vVariavel);
        lblValorAnterior.Caption := (FloatToStr(vConstante) + ' - ' + FloatToStr(vVariavel));
        fedNumeros.Text := FloatToStr(vResult);
      end;
    end;

    9: // SUBTRA��O COMPOSTA
    begin
      if vResult <> 0 then begin
        // Executa a opera��o
        vVariavel := StrToFloat(fedNumeros.Text);
        lblValorAnterior.Caption := FloatToStr(vResult) + ' - ' + FloatToStr(vVariavel);
        vResult := (vResult - vVariavel);
        fedNumeros.Text := FloatToStr(vResult);
      end;
    end;

    4: // SOMA SIMPLES
    begin
      if vResult <> 0 then begin
        lblValorAnterior.Caption := FloatToStr(vResult) + ' + ' + FloatToStr(vVariavel);
        vResult := vResult + vVariavel;
        fedNumeros.Text := FloatToStr(vResult);
      end;

      if vVariavel = 0 then begin
        // Passa Valor para a Vari�vel
        vVariavel := StrToFloat(fedNumeros.Text);
        // Executa a opera��o
        vResult := (vConstante + vVariavel);
        lblValorAnterior.Caption := (FloatToStr(vConstante) + ' + ' + FloatToStr(vVariavel));
        fedNumeros.Text := FloatToStr(vResult);
      end;
    end;

    5: // SOMA COMPOSTA
    begin
      If vResult <> 0 then begin

        if vAux = 0 then begin
          // Executa a opera��o
          vVariavel := StrToFloat(fedNumeros.Text);
          lblValorAnterior.Caption := FloatToStr(vResult) + ' + ' + FloatToStr(vVariavel);
          vResult := (vResult + vVariavel);
          fedNumeros.Text := FloatToStr(vResult);
          vAux := 1;
        end

        else begin
          // Duplo Enter depois da soma
          vResult := vResult + vVariavel;
          fedNumeros.Text := FloatToStr(vResult);
        end;

      end;

    end;

    6:
    begin
      if vVariavel = 0 then begin
        vVariavel := StrToFloat(fedNumeros.Text);
      end;

      // Executa a opera��o
      vResult := (vConstante / 100) * vVariavel;
      fedNumeros.Text := FloatToStr(vResult);
    end;
  end;
end;

procedure TfrmCalculadora.btnLimpaClick(Sender: TObject);
begin
  inherited;
  lblValorAnterior.Caption := '';
  fedNumeros.Clear;
  vConstante := 0;
  vVariavel := 0;
  vResult := 0;
end;

procedure TfrmCalculadora.btnMultiplicacaoClick(Sender: TObject);
begin
  inherited;
  if fedNumeros.Text = '' then begin
    fedNumeros.SetFocus;
    Abort;
  end;

  vConstante := StrToFloat(fedNumeros.Text);
  vVariavel := 0;
  fedNumeros.Text := '';
  vOperacao := 2;
  lblValorAnterior.Caption := FloatToStr(vResult);
end;

procedure TfrmCalculadora.btnNoveClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + (Sender as TSpeedButton).Caption;
end;

procedure TfrmCalculadora.btnOitoClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + (Sender as TSpeedButton).Caption;
end;

procedure TfrmCalculadora.btnPorcentagemClick(Sender: TObject);
begin
  inherited;
  if fedNumeros.Text = '' then begin
    fedNumeros.SetFocus;
    Abort;
  end;

  vConstante := StrToFloat(fedNumeros.Text);


  vVariavel := 0;
  fedNumeros.Text := '';
  vOperacao := 6;
  lblValorAnterior.Caption := FloatToStr(vResult);
end;

procedure TfrmCalculadora.btnQuatroClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + (Sender as TSpeedButton).Caption;
end;

procedure TfrmCalculadora.btnSeisClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + (Sender as TSpeedButton).Caption;
end;

procedure TfrmCalculadora.btnSeteClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + (Sender as TSpeedButton).Caption;
end;

procedure TfrmCalculadora.btnSomaClick(Sender: TObject);
begin
  inherited;

  // Vari�vel para corrigir Enter sobre enter
  vAux := 0;

  if fedNumeros.Text = '' then begin
    fedNumeros.SetFocus;
    Abort;
  end;

  // Constante recebe o valor informado no Edit
  vConstante := StrToFloat(fedNumeros.Text);

  // Zera o Edit
  fedNumeros.Text := '';

  // Passa o valor para a Label em tempo real
  lblValorAnterior.Caption := FloatToStr(vConstante) + ' + ';

  if vResult <> 0 then begin
    vOperacao := 5;
  end;

  if fedNumeros.Text <> '' then begin
    // SOMA SIMPLES
    vConstante := vConstante + StrToFloat(fedNumeros.Text);
    fedNumeros.Text := FloatToStr(vConstante);
    lblValorAnterior.Caption := FloatToStr(vConstante) + ' + ' + fedNumeros.Text;
    vOperacao := 4;
  end;
end;

procedure TfrmCalculadora.btnSubtracaoClick(Sender: TObject);
begin
  inherited;
  if fedNumeros.Text = '' then begin
    fedNumeros.SetFocus;
    Abort;
  end;

  // Constante recebe o valor informado no Edit
  vConstante := StrToFloat(fedNumeros.Text);

  // Zera o Edit
  fedNumeros.Text := '';

  // Passa o valor para a Label em tempo real
  lblValorAnterior.Caption := FloatToStr(vConstante) + ' - ';

  if vVariavel = 0 then begin
    // SOMA SIMPLES
    vOperacao := 3;
  end;
end;

procedure TfrmCalculadora.btnTresClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + (Sender as TSpeedButton).Caption;
end;

procedure TfrmCalculadora.btnUmClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + (Sender as TSpeedButton).Caption;
end;

procedure TfrmCalculadora.btnVirgulaClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + ',';
end;

procedure TfrmCalculadora.btnZeroClick(Sender: TObject);
begin
  inherited;
  fedNumeros.Text := fedNumeros.Text + (Sender as TSpeedButton).Caption;
end;


procedure TfrmCalculadora.FormCreate(Sender: TObject);
// Zera as vari�veis na Cria��o do Formul�rio
begin
  inherited;
  vConstante := 0;
  vVariavel  := 0;
end;

procedure TfrmCalculadora.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  // Bindar as teclas no key down
//  if Key = '4' then
//     ShowMessage(key);

  case Key of

    '0':
    begin
      btnZero.Click;
    end;

    '1':
    begin
      btnUm.Click;
    end;

    '2':
    begin
      btnDois.Click;
    end;

    '3':
    begin
      btnTres.Click;
    end;

    '4':
    begin
      btnQuatro.Click;
    end;

    '5':
    begin
      btnCinco.Click;
    end;

    '6':
    begin
      btnSeis.Click;
    end;

    '7':
    begin
      btnSete.Click;
    end;

    '8':
    begin
      btnOito.Click;
    end;

    '9':
    begin
      btnNove.Click;
    end;

    '/':
    begin
      btnDivisao.Click;
    end;

    '*':
    begin
      btnMultiplicacao.Click;
    end;

    '-':
    begin
      btnSubtracao.Click;
    end;

    '+':
    begin
      btnSoma.Click;
    end;

    '%':
    begin
      btnPorcentagem.Click;
    end;

    #13, '=':
    begin
      btnEnter.Click;
    end;
  end;
end;

end.
